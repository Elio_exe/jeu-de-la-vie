#!/usr/bin/python3
# coding: utf-8

from tkinter import *

import numpy as np
import random

#(((((((((((((((((((((((((((((((( Definition des fonctions et methodes ))))))))))))))))))))))))))))))))

def Vitesse():
    vitesse=1000/s3.get()   # recupere la valeur de s3 --> vitesse de l'afffichage mis a jour

def Taille():
    global c
    N=s1.get()      # recupere la valeur de s1 --> taille de la grille
    c=725/int(N)
    fenetre.after(1, Taille)

def init_matrice():      #Le tableau est composé que de 0 pour l'instant
    global matrice
    global N
    N=s1.get()
    matrice=np.zeros((N,N))


def aleatoire():
    global x
    x=s2.get()         # recupere la valeur de s2 --> pourcentage de vie
    global matrice
    global N
    cpt=0              #compteur du nombre de 1 (de vies)
    proba = x*N*N/100
    while cpt<proba:
        for i in range(N):
            for j in range(N):
                a = random.randint(0, 100)
                if (a<=x and matrice[i,j]==0 and cpt<proba):
                    matrice[i,j] = 1
                    cpt=cpt+1

def liste1():       # range les informations de la matrice dans une liste
    global matrice
    global N
    global l1
    l1=[]
    for i in range(N):
            for j in range(N):
                l1.append(matrice[i,j])



def aff_rect():     # affiche les rectangles dans le canvas jeu
    global c
    global l1
    l2=[]
    for i in np.arange(5,730,c):
        for j in np.arange(5,730,c):
            l2.append(jeu.create_rectangle(j,i,j+c,i+c, outline='black', width='1', fill='white'))
    for k in range(0,len(l1),1):
        if l1[k]==1  :
            jeu.itemconfigure(l2[k], fill='red')
        elif l1[k]==0 :
            jeu.itemconfigure(l2[k], fill='white')
    return l2


def compter_voisins(matrice, i, j):
    if (i==0):
        if (j==0):
            return(matrice[i,j+1]+matrice[i+1, j]+matrice[i+1,j+1])
        elif(j==N-1):
            return(matrice[i,j-1]+matrice[i+1,j-1]+matrice[i+1, j])
        else:
            return(matrice[i,j-1]+matrice[i+1,j-1]+matrice[i+1, j]+matrice[i+1,j+1]+matrice[i,j+1])
    elif(i==N-1):
        if(j==0):
            return(matrice[i-1, j]+matrice[i-1,j+1]+matrice[i,j+1])
        elif(j==N-1):
            return(matrice[i-1, j]+matrice[i-1,j-1]+matrice[i,j-1])
        else:
            return(matrice[i,j-1]+matrice[i-1,j-1]+matrice[i-1, j]+matrice[i-1,j+1]+matrice[i,j+1])
    else:
        if (j==0):
            return(matrice[i-1, j]+matrice[i-1,j+1]+matrice[i,j+1]+matrice[i+1,j+1]+matrice[i+1, j])
        elif(j==N-1):
            return(matrice[i-1, j]+matrice[i-1,j-1]+matrice[i,j-1]+matrice[i+1,j-1]+matrice[i+1, j])
        else:
            return(matrice[i-1, j]+matrice[i-1,j+1]+matrice[i,j+1]+matrice[i+1,j+1]+matrice[i+1, j]+matrice[i+1,j-1]+matrice[i,j-1]+matrice[i-1,j-1])


def iterer_jeu():
    global matrice
    global l2
    global flag
    if flag==1:
        precG = matrice.copy()
        l2=aff_rect()
        l3=[]
        for i in range(N):
            for j in range(N):
                c = compter_voisins(precG, i, j)
                if ((matrice[i,j] == 1 and c == 2) or c == 3):
                    matrice[i,j] = 1
                    l3.append(1)
                else:
                    matrice[i,j] = 0
                    l3.append(0)
        for k in range(0,len(l3),1):
            if l3[k]==1 :
                jeu.itemconfigure(l2[k], fill='red')
            elif l3[k]==0  :
                jeu.itemconfigure(l2[k], fill='white')
        t=s3.get()
        t=1000//t
        fenetre.after(t, iterer_jeu)


def initialiser():
    Arret()
    jeu.delete('all')
    init_matrice()
    aleatoire()
    liste1()
    aff_rect()


def Arret():
    global flag
    flag=0


def Lancer():
    global flag
    flag=1
    iterer_jeu()


#(((((((((((((((((((((((((((((( Programme Principal ))))))))))))))))))))))))))))))

fenetre = Tk()
fenetre.title("SR01 JEU DE LA VIE")
fenetre.config(height=735,width=1003, background='#E4E4E4' )

# Les 2 canvas :
menu=Canvas(fenetre, width=300, height=1000, background='#E4E4E4', bd=0)
jeu=Canvas(fenetre, width=730, height=730, background='white')

# Les 4 boutons :
b1=Button(menu, text="QUITTER",fg="blue", width=29, height=3, command=fenetre.destroy)
b2=Button(menu,text="LANCER",fg="blue", width=29, height=3, command=Lancer)
b3=Button(menu,text="ARRETER",fg="blue", width=29, height=3, command=Arret)
b4=Button(menu,text="INITIALISER",fg="blue", width=29, height=3, command=initialiser)

# Les 3 echelles :
s1=Scale(menu, label="TAILLE DE LA GRILLE",fg="blue", orient='horizontal', length=150, bg='#E4E4E4', from_=1, to_=100, command=Taille)
s2=Scale(menu, label="% DE VIE",fg="blue", orient='horizontal', length=150, bg='#E4E4E4', command=aleatoire)
s3=Scale(menu, label="VITESSE", fg="blue", orient='horizontal', length=150, from_=1, to_=1000, bg='#E4E4E4', command=Vitesse)

Taille() # on apelle Taille une premiere fois pour lui permettre de boucler (after())

r1=jeu.create_rectangle(5,5,730,730, outline='black', width='1')

# On utilise .place car nous avons rencontre plusieurs problemes avec .pack
b1.place(x=37,y=690)
b2.place(x=37,y=11)
b3.place(x=37,y=66)
b4.place(x=37,y=121)

s1.place(x=80,y=350)
s2.place(x=80,y=450)
s3.place(x=80,y=550)

menu.place(x=700, y=-10)
jeu.place(x=0, y=0)

fenetre.mainloop()
